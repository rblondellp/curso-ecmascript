const anotherFunction = () => {
    return new Promise((resolve, reject) => {
        if (false) {
            resolve("Ok!!");
        } else {
            reject("Pf!!");
        }
    });
};

anotherFunction()
    .then(response => console.log(response))
    .catch(err => console.log(err))
    .finally(() => console.log("Finally"));
