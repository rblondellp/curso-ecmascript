function newUser(name, age, country) {
    var name = name || "Ray";
    var age = age || 25;
    var country = country || "Venezuela";

    console.log(name, age, country);
}
newUser();
newUser("Vicky", 24, "VE");

function newAdmin(name = "Ray", age = 25, country = "VE") {
    console.log(name, age, country);
}
newAdmin();
newAdmin("Vicky", 24, "VENEZUELA");
