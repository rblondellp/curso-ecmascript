//object literals

function newUser(user, age, country, uId) {
    return {
        user,
        age,
        country,
        id: uId,
    };
}

console.log(newUser("ray", 25, "VE", 1));
