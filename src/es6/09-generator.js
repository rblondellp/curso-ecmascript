function* iterate(array) {
    for (let value of array) {
        yield value;
    }
}

const it = iterate(["ray", "vicky", "alex", "ondriel"]);
console.log(it.next().value);
console.log(it.next().value);
console.log(it.next().value);
console.log(it.next().value);
