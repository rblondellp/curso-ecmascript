// declarando
class User {}

//instancia de una clase
// const newUser = new User();

class user {
    //metodos
    greeting() {
        return "Hello";
    }
}

const ray = new user();
console.log(ray.greeting());
const developer = new user();
console.log(developer.greeting());

//constructor
class user {
    //Constructor
    constructor() {
        console.log("Nuevo usuario");
    }

    greeting() {
        return "Hello";
    }
}

const vicky = new user();

//this
class user {
    constructor(name) {
        this.name = name;
    }
    //metodos
    speak() {
        return "Hello";
    }

    greeting() {
        return `${this.speak()} ${this.name}`;
    }
}

const alex = new user("alex");
console.log(alex.greeting());

// settears getters
class user {
    constructor(name, age) {
        this.anem = name;
        this.age = age;
    }
    //metodos
    speak() {
        return "Hello";
    }

    greeting() {
        return `${this.speak()} ${this.name}`;
    }

    get uAge() {
        return this.age;
    }

    set uAge(n) {
        this.age = n;
    }
}

const maria = new user("Maria", 55);
console.log(maria.uAge);
console.log((maria.uAge = 56));
