let hello = 'Hello';
let world = 'World';
let epicPhrase = hello + ' ' + world;

//template literals
let epicPhrase2 = `${hello} ${world}!`;
console.log(epicPhrase2);

// multi line strings
let lorem = 'esto es un string \n' + 'esta es otra linea';

let lorem2 = `esta es una frace epica
    continuacion de esa frase epica
`;
console.log(lorem);
console.log(lorem2);
