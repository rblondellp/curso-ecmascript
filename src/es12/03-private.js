class user {
    constructor(name, age) {
        this.anem = name;
        this.age = age;
    }
    //metodos
    #speak() {
        return "Hello";
    }

    greeting() {
        return `${this.speak()} ${this.name}`;
    }

    get #uAge() {
        return this.age;
    }

    set #uAge(n) {
        this.age = n;
    }
}

const maria = new user("Maria", 55);
console.log(maria.uAge);
console.log((maria.uAge = 56));
